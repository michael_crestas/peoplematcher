﻿using System;
using System.Collections.Generic;
using Faker;
using Faker.Selectors;
using PeopleMatcher.Annotations;
using PeopleMatcher.Models;

namespace PeopleMatcher.Test
{
    public static class TestUtilities
    {
        [NotNull]
        public static Person GetPerson()
        {
            var faker = GetPersonFaker();
            return faker.Generate();
        }

        [NotNull]
        public static IList<Person> GetPeople(int count, JobRole jobRole = JobRole.Unspcified, JobRole preference = JobRole.Unspcified)
        {
            var faker = GetPersonFaker();
            var people = faker.Generate(count);
            if (jobRole != JobRole.Unspcified) people.ForEach(person => person.JobRole = jobRole);
            if (preference != JobRole.Unspcified) people.ForEach(person => person.Preference = preference);
            return people;
        }

        public static void GetPersonAndPeople(out Person person, out IList<Person> people, int count)
        {
            person = GetPerson();
            people = GetPeople(count);
        }

        [NotNull]
        private static Fake<Person> GetPersonFaker()
        {
            var faker = new Fake<Person>();
            faker.AddSelector(new JobRoleSelector());
            return faker;
        }
    }

    internal class JobRoleSelector : TypeSelectorBase<JobRole>
    {
        private readonly Random _random = new Random();

        public override JobRole Generate()
        {
            var values = Enum.GetValues(typeof (JobRole));
            var num = _random.Next(0, values.Length - 1);

            return (JobRole) values.GetValue(num);
        }
    }
}