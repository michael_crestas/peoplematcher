﻿using NUnit.Framework;
using PeopleMatcher.Models;
using PeopleMatcher.Rules;

namespace PeopleMatcher.Test
{
    public class RuleTests
    {
        protected static void TestCorrectWeight(Rule rule, Person person, Person match, double expectedWeight)
        {
            var weight = rule.GetWeight(person, match);
            Assert.AreEqual(expectedWeight, weight);
        }
    }
}