﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using PeopleMatcher.Models;
using PeopleMatcher.Rules;

namespace PeopleMatcher.Test
{
    [TestFixture]
    public class JobRoleRuleTests : RuleTests
    {
        private List<Tuple<JobRole, JobRole, double>> GetTestData()
        {
            return new List<Tuple<JobRole, JobRole, double>>
            {
                Tuple.Create(JobRole.BusinessAnalyst, JobRole.BusinessAnalyst, 1.0),
                Tuple.Create(JobRole.BusinessAnalyst, JobRole.Developer, 0.0),
                Tuple.Create(JobRole.BusinessAnalyst, JobRole.BusinessAnalyst | JobRole.ProjectManager, 1.0),
                Tuple.Create(JobRole.BusinessAnalyst, JobRole.ProjectManager | JobRole.BusinessAnalyst, 1.0),
            };
        }

        [Test]
        [TestCaseSource("GetTestData")]
        public void GetWeight(Tuple<JobRole, JobRole, double> parameters)
        {
            var people = TestUtilities.GetPeople(2, parameters.Item1, parameters.Item2);
            TestCorrectWeight(new JobRoleRule(1), people[0], people[1], parameters.Item3);
        }
    }
}