﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using PeopleMatcher.Rules;

namespace PeopleMatcher.Test
{
    [TestFixture]
    public class YearGroupRuleTests : RuleTests
    {
        private static List<Tuple<int, int, double>> GetTestData()
        {
            return new List<Tuple<int, int, double>>
            {
                Tuple.Create(2014, 2014, 0*Math.Exp(0)/Math.Exp(0)),
                Tuple.Create(2014, 2013, 1*Math.Exp(-1)/Math.Exp(-1)),
                Tuple.Create(2014, 2012, 2*Math.Exp(-2)/Math.Exp(-1)),
                Tuple.Create(2014, 2011, 3*Math.Exp(-3)/Math.Exp(-1)),
                Tuple.Create(2014, 2010, 4*Math.Exp(-4)/Math.Exp(-1)),
                Tuple.Create(2014, 2009, 5*Math.Exp(-5)/Math.Exp(-1)),
                Tuple.Create(2009, 2014, 5*Math.Exp(-5)/Math.Exp(-1))
            };
        }

        [Test]
        [TestCaseSource("GetTestData")]
        public void GetWeight_DifferentYearGroups(Tuple<int, int, double> parameters)
        {
            var people = TestUtilities.GetPeople(2);
            people[0].YearGroup = parameters.Item1;
            people[1].YearGroup = parameters.Item2;

            TestCorrectWeight(new YearGroupRule(1), people[0], people[1], parameters.Item3);
        }

        [Test]
        public void GetWeight_MatchMissingYearGroup()
        {
            var people = TestUtilities.GetPeople(2);
            people[1].YearGroup = null;
            TestCorrectWeight(new YearGroupRule(1), people[0], people[1], 0);
        }

        [Test]
        public void GetWeight_PersonMissingYearGroup()
        {
            var people = TestUtilities.GetPeople(2);
            people[0].YearGroup = null;
            TestCorrectWeight(new YearGroupRule(1), people[0], people[1], 0);
        }

        [Test]
        public void GetWeight_ZeroYearDifference()
        {
            var people = TestUtilities.GetPeople(2);
            people.ForEach(person => person.YearGroup = 2014);

            TestCorrectWeight(new YearGroupRule(1), people[0], people[1], 0);
        }
    }
}