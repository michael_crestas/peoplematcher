﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Schema;
using PeopleMatcher.Models;

namespace PeopleMatcher
{
    public class PersonTextMatchPersistor : IPersistor<Person>
    {
        private readonly ISerializer<Person> _personSerializer;
        private readonly string _filename;


        public PersonTextMatchPersistor(ISerializer<Person> personSerializer, string filename)
        {
            _personSerializer = personSerializer;
            _filename = filename;
        }

        public void Save(Person data)
        {
            using (var fs = new FileStream(_filename, FileMode.Create, FileAccess.Write))
            {
                using (var tx = new StreamWriter(fs))
                {
                    tx.WriteLine(_personSerializer.Serialize(data));
                }
            }
        }

        public void Save(IEnumerable<Person> data)
        {
            using (var fs = new FileStream(_filename, FileMode.Create, FileAccess.Write))
            {
                using (var tx = new StreamWriter(fs))
                {
                    foreach (var match in data)
                    {
                        tx.WriteLine(_personSerializer.Serialize(match));
                    }
                }
            }
        }

        public bool TryRetrieve(out IEnumerable<Person> data)
        {
            var list = new List<Person>();
            if (!File.Exists(_filename))
            {
                data = list;
                return false;
            }

            using (var fs = new FileStream(_filename, FileMode.Open, FileAccess.Read))
            {
                using (var stream = new StreamReader(fs))
                {
                    string line;
                    while ((line = stream.ReadLine()) != null)
                    {
                        list.Add(_personSerializer.Deserialize(line));
                    }
                }
            }

            data = list;
            return true;
        }

        public void Append(Person data)
        {
            using (var fs = new FileStream(_filename, FileMode.Append, FileAccess.Write))
            {
                using (var tx = new StreamWriter(fs))
                {
                    tx.WriteLine(_personSerializer.Serialize(data));
                }
            }
        }

        public void Append(IEnumerable<Person> data)
        {
            using (var fs = new FileStream(_filename, FileMode.Append, FileAccess.Write))
            {
                using (var tx = new StreamWriter(fs))
                {
                    foreach (var match in data)
                    {
                        tx.WriteLine(_personSerializer.Serialize(match));
                    }
                }
            }
        }

        public void Complete()
        {
        }
    }
}