using System;
using System.Collections.Generic;
using System.Linq;
using PeopleMatcher.Models;

namespace PeopleMatcher.MatchProvider
{
    public abstract class MatchProviderBase : IMatchProvider
    {
        protected IList<Match> ExistingMatches;
        private IList<Person> _matchedPeople;
        private IEnumerable<Person> _people;

        public IEnumerable<Match> GetMatches(Person[] people, IList<Match> existingMatches)
        {
            _people = people;
            _matchedPeople = new List<Person>();
            ExistingMatches = existingMatches;

            var matches = new List<Match>();

            foreach (var person in people)
            {
                Match match;
                if (TryMatch(person, out match))
                {
                    matches.Add(match);
                    RegisterMatch((match));
                }
            }

            return matches;
        }

        private bool TryMatch(Person person, out Match match)
        {
            match = null;

            if (_matchedPeople.Contains(person))
            {
                return false;
            }

            var possibleMatches = GetEligibleMatches(person).ToList();

            if (possibleMatches.Count < 1)
            {
                return false;
            }

            var rand = new Random();
            var index = rand.Next(possibleMatches.Count - 1);

            match = possibleMatches[index];

            return true;
        }

        private void RegisterMatch(Match match)
        {
            _matchedPeople.Add(match.FirstPerson);
            _matchedPeople.Add(match.SecondPerson);
            ExistingMatches.Add(match);
        }

        protected IEnumerable<Person> GetUnmatchedPeople()
        {
            return _people.Where(person => !_matchedPeople.Contains(person));
        }

        protected abstract IEnumerable<Match> GetEligibleMatches(Person person);
    }
}