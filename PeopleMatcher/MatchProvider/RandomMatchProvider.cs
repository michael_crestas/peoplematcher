using System.Collections.Generic;
using System.Linq;
using PeopleMatcher.Models;

namespace PeopleMatcher.MatchProvider
{
    internal class RandomMatchProvider : MatchProviderBase
    {
        protected override IEnumerable<Match> GetEligibleMatches(Person person)
        {
            var unmatchedPeople = GetUnmatchedPeople();
            
            return unmatchedPeople
                .Where(p => !p.Equals(person))
                .Select(p => new Match(person, p))
                .Where(match => !ExistingMatches.Contains(match));
        }
    }
}