﻿using System.Collections.Generic;
using PeopleMatcher.Models;

namespace PeopleMatcher.MatchProvider
{
    public interface IMatchProvider
    {
        IEnumerable<Match> GetMatches(
            Person[] people,
            IList<Match> existingMatches);
    }
}