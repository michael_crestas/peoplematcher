﻿using System.Collections.Generic;
using System.Linq;
using PeopleMatcher.Models;

namespace PeopleMatcher.MatchProvider
{
    public class BackupMatchProvider : MatchProviderBase
    {
        private readonly List<Person> _backupPeople; 

        public BackupMatchProvider()
        {
            _backupPeople = new List<Person>()
            {
                new Person("Hassan", "Munir") {JobRole = JobRole.Developer},
                new Person("Jayne", "Vickery") {JobRole = JobRole.BusinessAnalyst},
                new Person("Mike", "Crestas") {JobRole = JobRole.Developer}
            };
        }

        protected override IEnumerable<Match> GetEligibleMatches(Person person)
        {
            return _backupPeople
                .Randomize()
                .Where(p => !p.Equals(person))
                .Select(p => new Match(person, p))
                .Where(match => !ExistingMatches.Contains(match));
        }
    }
}