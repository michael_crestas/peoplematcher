﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using PeopleMatcher.Models;

namespace PeopleMatcher.MatchProvider
{
    public class CompositeMatchProvider : IMatchProvider
    {
        public IMatchProvider[] MatchProviders { get; set; }

        public IEnumerable<Match> GetMatches(
            Person[] people,
            IList<Match> existingMatches)
        {
            IList<Match> matches = new List<Match>();

            if (!IsInputValid(people)) return matches;

            IList<Person> matchedPeople = new List<Person>();

            foreach (var matchProvider in MatchProviders)
            {
                Trace.TraceInformation("Matching people using {0}", matchProvider.GetType().Name);

                var newMatches = matchProvider
                    .GetMatches(people, existingMatches)
                    .ToList();

                
                newMatches.ForEach(
                    match => RegisterMatch(match, ref matches, ref matchedPeople, ref existingMatches));

                people = people.Except(matchedPeople).ToArray();

                PrintDetails(newMatches, matchProvider.GetType().Name);
            }

            return matches;
        }

        private static void RegisterMatch(
            Match newMatch,
            ref IList<Match> newMatches,
            ref IList<Person> matchedPeople,
            ref IList<Match> existingMatches)
        {
            newMatches.Add(newMatch);
            matchedPeople.Add(newMatch.FirstPerson);
            matchedPeople.Add(newMatch.SecondPerson);
            existingMatches.Add(newMatch);
        }

        private static void PrintDetails<T>(IEnumerable<T> data, string message)
        {
            var list = data.ToList();
            if (list.Count > 0)
            {
                Trace.WriteLine(message);
                Trace.Indent();
                list.ForEach(obj => Trace.WriteLine(obj));
                Trace.Unindent();
            }
        }

        private bool IsInputValid(IEnumerable<Person> people)
        {
            if (!MatchProviders.Any())
            {
                Trace.TraceWarning("No MatchProvider specified.");
                return false;
            }

            if (!people.Any())
            {
                Trace.TraceWarning("No people specified.");
                return false;
            }

            return true;
        }
    }
}