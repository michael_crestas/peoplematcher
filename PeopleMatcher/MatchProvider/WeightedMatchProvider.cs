﻿using System.Collections.Generic;
using System.Linq;
using PeopleMatcher.Annotations;
using PeopleMatcher.Models;
using PeopleMatcher.Rules;

namespace PeopleMatcher.MatchProvider
{
    public class WeightedMatchProvider : IMatchProvider
    {
        public WeightedMatchProvider()
        {
        }

        public WeightedMatchProvider([NotNull] Rule rule, double weightThreshold)
        {
            Rule = rule;
            WeightThreshold = weightThreshold;
        }

        public double WeightThreshold { get; set; }

        [NotNull]
        public Rule Rule { get; set; }

        [NotNull]
        public IEnumerable<Match> GetMatches([NotNull] Person[] people, [NotNull] IList<Match> existingMatches)
        {
            var matches = new List<Match>();
            var matchEngine = new MatchEngine(Rule, WeightThreshold);
            var matchedPeople = new List<Person>();
            var newMatches = new List<Match>();

            foreach (var person in people)
            {
                var p = person;
                if (matchedPeople.Contains(p)) continue;

                var peopleMatchedWithAlready = GetPeopleMatchedAlready(existingMatches, p);
                peopleMatchedWithAlready.Add(p); // Add myself to the list so I don't try to create a match with myself

                var eligiblePeople = people
                    .Except(peopleMatchedWithAlready)
                    .Except(matchedPeople)
                    .ToList();

                var foundMatch = matchEngine.GetMatch(p, eligiblePeople);
                if (foundMatch == null) continue;
                RegisterMatch(foundMatch, newMatches, matchedPeople, matches);
            }

            return matches;
        }

        private static List<Person> GetPeopleMatchedAlready(IEnumerable<Match> existingMatches, Person p)
        {
            var peopleMatchedWithAlready = existingMatches
                .Where(match => match.FirstPerson.Equals(p) || match.SecondPerson.Equals(p))
                .Select(match => match.FirstPerson.Equals(p) ? match.SecondPerson : match.FirstPerson)
                .Distinct()
                .ToList();
            return peopleMatchedWithAlready;
        }

        private static void RegisterMatch(
            Match foundMatch,
            ICollection<Match> existingMatches,
            ICollection<Person> matchedPeople,
            ICollection<Match> matches)
        {
            matchedPeople.Add(foundMatch.FirstPerson);
            matchedPeople.Add(foundMatch.SecondPerson);
            existingMatches.Add(foundMatch);
            matches.Add(foundMatch);
        }
    }
}