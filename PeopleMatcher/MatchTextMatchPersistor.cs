﻿using System;
using System.Collections.Generic;
using System.IO;
using PeopleMatcher.Models;

namespace PeopleMatcher
{
    public class MatchTextMatchPersistor : IPersistor<Match>
    {
        private readonly string _filename;
        private readonly ISerializer<Match> _matchSerializer;

        public MatchTextMatchPersistor(ISerializer<Match> matchSerializer, string filename)
        {
            _matchSerializer = matchSerializer;
            _filename = filename;
        }

        public void Save(Match data)
        {
            CreateDirectoryIfItDoesNotExist();

            using (var fs = new FileStream(_filename, FileMode.Create, FileAccess.Write))
            {
                using (var tx = new StreamWriter(fs))
                {
                    tx.WriteLine(_matchSerializer.Serialize(data));
                }
            }
        }

        public void Save(IEnumerable<Match> data)
        {
            CreateDirectoryIfItDoesNotExist();

            using (var fs = new FileStream(_filename, FileMode.Create, FileAccess.Write))
            {
                using (var tx = new StreamWriter(fs))
                {
                    foreach (var match in data)
                    {
                        tx.WriteLine(_matchSerializer.Serialize(match));
                    }
                }
            }
        }

        public bool TryRetrieve(out IEnumerable<Match> data)
        {
            var list = new List<Match>();
            if (!File.Exists(_filename))
            {
                data = list;
                return false;
            }


            using (var fs = new FileStream(_filename, FileMode.Open, FileAccess.Read))
            {
                using (var stream = new StreamReader(fs))
                {
                    string line;
                    while ((line = stream.ReadLine()) != null)
                    {
                        list.Add(_matchSerializer.Deserialize(line));
                    }
                }
            }

            data = list;
            return true;
        }

        public void Append(Match data)
        {
            using (var fs = new FileStream(_filename, FileMode.Append, FileAccess.Write))
            {
                using (var tx = new StreamWriter(fs))
                {
                    tx.WriteLine(_matchSerializer.Serialize(data));
                }
            }
        }

        public void Append(IEnumerable<Match> data)
        {
            using (var fs = new FileStream(_filename, FileMode.Append, FileAccess.Write))
            {
                using (var tx = new StreamWriter(fs))
                {
                    foreach (var match in data)
                    {
                        tx.WriteLine(_matchSerializer.Serialize(match));
                    }
                }
            }
        }

        public void Complete()
        {
        }

        private void CreateDirectoryIfItDoesNotExist()
        {
            if (string.IsNullOrWhiteSpace(_filename))
            {
                throw new ArgumentNullException(string.Format("File name not specified for {0}", GetType().FullName));
            }
            var directory = Path.GetDirectoryName(_filename);

            if (string.IsNullOrWhiteSpace(directory))
            {
                return;
            }

            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
        }
    }
}