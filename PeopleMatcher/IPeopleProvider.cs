﻿using System.Collections.Generic;
using PeopleMatcher.Models;

namespace PeopleMatcher
{
    internal interface IPeopleProvider
    {
        IEnumerable<Person> GetPeople();
    }
}