﻿using System;
using System.Collections.Generic;
using System.Linq;
using PeopleMatcher.Models;

namespace PeopleMatcher
{
    public class PeopleProvider : IPeopleProvider
    {
        private readonly IPersistor<Person> _persistor;

        public PeopleProvider(IPersistor<Person> persistor)
        {
            _persistor = persistor;
        }

        public IEnumerable<Person> GetPeople()
        {
            IEnumerable<Person> people;
            
            return !_persistor.TryRetrieve(out people) ? new List<Person>() : people;
        }
    }
}