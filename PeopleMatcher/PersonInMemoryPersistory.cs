using System;
using System.Collections.Generic;
using PeopleMatcher.Models;

namespace PeopleMatcher
{
    internal class PersonInMemoryPersistory : IPersistor<Person>
    {

        private List<Person> _personRepository; 

        public PersonInMemoryPersistory(ISerializer<Person> personSerializer)
        {
                _personRepository = new List<Person>();
        }

        public void Save(Person data)
        {
            _personRepository = new List<Person> {data};
        }

        public void Save(IEnumerable<Person> data)
        {
            _personRepository = new List<Person>();
            _personRepository.AddRange(data);
        }

        public bool TryRetrieve(out IEnumerable<Person> data)
        {
            data = _personRepository;
            return true;
        }

        public void Append(Person data)
        {
            _personRepository.Add(data);
        }

        public void Append(IEnumerable<Person> data)
        {
            _personRepository.AddRange(data);
        }

        public void Complete()
        {
            
        }
    }
}