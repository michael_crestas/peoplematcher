﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using PeopleMatcher.Annotations;

namespace PeopleMatcher
{
    public static class Extensions
    {
        [NotNull]
        public static IEnumerable<T> Randomize<T>([NotNull] this IEnumerable<T> source)
        {
            var rnd = new Random();
            return source.OrderBy(arg => rnd.Next());
        }

        public static void Shuffle<T>([NotNull] this IList<T> list, [NotNull] Random rnd)
        {
            for (var i = 0; i < list.Count; i++)
                list.Swap(i, rnd.Next(i, list.Count));
        }

        public static void Swap<T>([NotNull] this IList<T> list, int i, int j)
        {
            var temp = list[i];
            list[i] = list[j];
            list[j] = temp;
        }


        /// <summary>
        /// Performs an action for each element in the enumeration
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumeration">The enumeration.</param>
        /// <param name="action">The action to perform.</param>
        public static void ForEach<T>([NotNull] this IEnumerable<T> enumeration, [NotNull] Action<T> action)
        {
            foreach (var item in enumeration)
            {
                action(item);
            }
        }

        /// <summary>
        ///     Prints an introductory message then prints all data indented by calling ToString() on all objects
        /// </summary>
        /// <typeparam name="T">The object to print details of.</typeparam>
        /// <param name="enumeration">The data.</param>
        /// /// <param name="titleText">The introduction message.</param>
        public static void TraceDetails<T>([NotNull] this IEnumerable<T> enumeration, [NotNull] string titleText)
        {
            var list = enumeration.ToList();

            if (!list.Any()) return;

            Trace.WriteLine(titleText);
            Trace.Indent();
            foreach (var obj in list)
            {
                Trace.WriteLine(obj);
            }
            Trace.Unindent();
        }
    }
}