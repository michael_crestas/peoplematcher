﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using PeopleMatcher.MatchProvider;
using PeopleMatcher.Models;
using PeopleMatcher.Rules;
using YamlDotNet.Serialization;

namespace PeopleMatcher
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Trace.Listeners.Add(new ConsoleTraceListener(true));

            Configuration config;
            if (!TryGetConfiguration(args, out config))
            {
                Trace.TraceError("Invalid configuration. Usage: PeopleMatcher.exe [Configuration.yaml]");
                return;
            }

            // Create service providers
            ISerializer<Person> personSerializer = new PersonSerializer();
            ISerializer<Match> matchSerializer = new MatchSerializer(personSerializer);

            IPersistor<Person> personPersistor = new PersonTextMatchPersistor(
                personSerializer,
                config.PeopleInputFilePath);

            IPersistor<Person> unmatchedPersonPersistor = new PersonTextMatchPersistor(
                personSerializer,
                Path.Combine(config.OutputFileDirectory, "UnmatchedPeople.txt"));

            IPersistor<Match> existingMatchesPersistor = new MatchTextMatchPersistor(
                matchSerializer,
                Path.Combine(config.OutputFileDirectory, "Matches.txt"));

            IPersistor<Match> newMatchesPersistor = new MatchTextMatchPersistor(
                matchSerializer,
                Path.Combine(config.OutputFileDirectory, "NewMatches.txt"));

            IPeopleProvider peopleProvider = new PeopleProvider(personPersistor);
            //IPeopleProvider peopleProvider = new MockPeopleProvider();

            var people = peopleProvider.GetPeople().Randomize().ToList();

            var existingMatches = GetExistingMatches(existingMatchesPersistor);

            var newMatches = config.MatchProvider
                .GetMatches(people.ToArray(), existingMatches)
                .ToList();

            //var newMatches = matchProvider.GetMatches(people.ToArray(), existingMatches).ToList();
            var unmatchedPeople = GetUnmatchedPeople(newMatches, people);
            
            Trace.WriteLine(String.Empty);
            Trace.WriteLine(String.Format("Generated {0} matches for {1} people", newMatches.Count(), people.Count));

            Trace.WriteLine(String.Empty);
            newMatches.TraceDetails("All generated matches");

            Trace.WriteLine(String.Empty);
            unmatchedPeople.TraceDetails("Unmatched people");

            existingMatchesPersistor.Save(existingMatches);
            newMatchesPersistor.Save(newMatches);
            unmatchedPersonPersistor.Save(unmatchedPeople);

            PromptForKeyPress();
        }

        /// <summary>
        ///     Tries to the get configuration.
        /// </summary>
        /// <param name="args">Program arguments.</param>
        /// <param name="config">The configuration.</param>
        /// <returns></returns>
        private static bool TryGetConfiguration(string[] args, out Configuration config)
        {
            config = null;

            if (!args.Any())
            {
                return false;
            }

            try
            {
                var deserializer = new Deserializer();
                deserializer.RegisterTagMapping("!CompositeMatchProvider", typeof (CompositeMatchProvider));
                deserializer.RegisterTagMapping("!RandomMatchProvider", typeof (RandomMatchProvider));
                deserializer.RegisterTagMapping("!WeightedMatchProvider", typeof(WeightedMatchProvider));
                deserializer.RegisterTagMapping("!AggregateRule", typeof(AggregateRule));
                deserializer.RegisterTagMapping("!RandomRule", typeof(RandomRule));
                deserializer.RegisterTagMapping("!JobRoleRule", typeof(JobRoleRule));
                deserializer.RegisterTagMapping("!YearGroupRule", typeof(YearGroupRule));
                deserializer.RegisterTagMapping("!BackupMatchProvider", typeof (BackupMatchProvider));
                config = deserializer.Deserialize<Configuration>(new StreamReader(args[0]));
                return true;
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.Message);
                return false;
            }
        }

        /// <summary>
        ///     Gets the existing matches.
        /// </summary>
        /// <param name="existingMatchesPersistor">The existing matches persistor.</param>
        /// <returns>A <c>IList</c> of <c>existing matches</c></returns>
        private static IList<Match> GetExistingMatches(IPersistor<Match> existingMatchesPersistor)
        {
            IEnumerable<Match> existingMatches;
            existingMatchesPersistor.TryRetrieve(out existingMatches);
            return existingMatches.ToList();
        }

        private static IList<Person> GetUnmatchedPeople(IEnumerable<Match> matches, IEnumerable<Person> people)
        {
            var matchedPeople = new List<Person>();
            foreach (var match in matches)
            {
                matchedPeople.Add(match.FirstPerson);
                matchedPeople.Add(match.SecondPerson);
            }

            return people.Except(matchedPeople).ToList();
        }

        /// <summary>
        ///     Prompts and waits for a key press
        /// </summary>
        private static void PromptForKeyPress()
        {
            Console.WriteLine("Press any key to exit..");
            Console.ReadLine();
        }
    }
}