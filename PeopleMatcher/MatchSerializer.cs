﻿using System;
using System.Linq;
using PeopleMatcher.Models;

namespace PeopleMatcher
{
    public class MatchSerializer : ISerializer<Match>
    {
        private readonly ISerializer<Person> _personSerializer;

        public MatchSerializer(ISerializer<Person> personSerializer)
        {
            _personSerializer = personSerializer;
        }

        public string Serialize(Match obj)
        {
            return String.Format("{0} \t | \t {1}",
                _personSerializer.Serialize(obj.FirstPerson),
                _personSerializer.Serialize(obj.SecondPerson));
        }

        public Match Deserialize(string str)
        {
            str = str.Trim();
            var arr = str.Split('|');
            var trimmed = arr.Select(s => s.Trim()).ToArray();
            if (trimmed.Count() != 2)
            {
                throw new ArgumentException("Invalid string");
            }

            var firstPerson = _personSerializer.Deserialize(trimmed[0]);
            var secondPerson = _personSerializer.Deserialize(trimmed[1]);

            return new Match(firstPerson, secondPerson);
        }
    }
}