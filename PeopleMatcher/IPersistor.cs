using System.Collections.Generic;

namespace PeopleMatcher
{
    public interface IPersistor<T>
    {
        void Save(T data);

        void Save(IEnumerable<T> data);

        bool TryRetrieve(out IEnumerable<T> data);

        void Append(T data);

        void Append(IEnumerable<T> data);

        void Complete();

    }
}