﻿using System;
using System.Runtime.Serialization;

namespace PeopleMatcher.Models
{
    [DataContract]
    public class Person : IEquatable<Person>
    {
        public Person()
        {
        }

        public Person(string firstName, string lastName)
        {
            FirstName = firstName.Trim();
            LastName = lastName.Trim();
        }

        [DataMember]
        public string FirstName { get; private set; }

        [DataMember]
        public string LastName { get; private set; }

        [DataMember]
        public JobRole Preference { get; set; }

        [DataMember]
        public JobRole JobRole { get; set; }

        [DataMember]
        public int? YearGroup { get; set; }

        [DataMember]
        public string Email { get; set; }

        public string FullName
        {
            get { return String.Format("{0} {1}", FirstName, LastName); }
        }
        
        public bool Equals(Person other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return string.Equals(FirstName, other.FirstName) && string.Equals(LastName, other.LastName);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Person) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((FirstName != null ? FirstName.GetHashCode() : 0)*397) ^
                       (LastName != null ? LastName.GetHashCode() : 0);
            }
        }

        public override string ToString()
        {
            return String.Format("{0}, {1}", LastName, FirstName);
        }
    }
}