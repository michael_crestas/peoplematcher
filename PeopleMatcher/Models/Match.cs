﻿using System;
using System.Runtime.Serialization;

namespace PeopleMatcher.Models
{
    [DataContract]
    public class Match :  IEquatable<Match>
    {
        public Match(Person firstPerson, Person secondPerson)
        {
            if (firstPerson == secondPerson)
                throw new ArgumentException("Cannot make a match between the same people!");

            FirstPerson = firstPerson;
            SecondPerson = secondPerson;
        }

        [DataMember]
        public Person FirstPerson { get; private set; }

        [DataMember]
        public Person SecondPerson { get; private set; }

        public double Weight { get; set; }

        public bool Equals(Match other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return
                (FirstPerson.Equals(other.FirstPerson) || FirstPerson.Equals(other.SecondPerson)) &&
                (SecondPerson.Equals(other.SecondPerson) || SecondPerson.Equals(other.FirstPerson));
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Match) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (FirstPerson.GetHashCode()*397) ^ SecondPerson.GetHashCode();
            }
        }

        public override string ToString()
        {
            return String.Format("{0, -25}{1,-25}{2,-25}", FirstPerson, SecondPerson, Weight);
        }
    }
}