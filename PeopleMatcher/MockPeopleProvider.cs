using System.Collections.Generic;
using PeopleMatcher.Models;

namespace PeopleMatcher
{
    internal class MockPeopleProvider : IPeopleProvider
    {
        public IEnumerable<Person> GetPeople()
        {
            return new List<Person>
            {
                new Person("Hassan", "Munir") {JobRole = JobRole.Developer},
                new Person("Emily", "Williams") {JobRole = JobRole.BusinessAnalyst, Preference = JobRole.BusinessAnalyst},
                new Person("Tom", "Bombadill") {Preference = JobRole.Developer},
                new Person("Frodo", "Baggins"){JobRole = JobRole.BusinessAnalyst},
                new Person("Albus", "Dumbledore"){JobRole = JobRole.Developer, Preference = JobRole.Developer},
                new Person("Bathilda", "Bagshot")
            };
        }
    }
}