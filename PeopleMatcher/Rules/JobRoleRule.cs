﻿using System.Runtime.Remoting.Messaging;
using PeopleMatcher.Models;

namespace PeopleMatcher.Rules
{
    public class JobRoleRule : Rule
    {
        public JobRoleRule()
        {
        }

        public JobRoleRule(double weighting)
        {
            Weighting = weighting;
        }

        public override double GetWeight(Person person, Person match)
        {
            if (person.Preference == JobRole.Unspcified)
            {
                if (match.Preference.HasFlag(person.JobRole)) return 1;
            }

            if (person.Preference.HasFlag(match.JobRole)) return 1;     // If my preference is other persons job
            if (person.JobRole.HasFlag(match.Preference)) return 0.5;    // If my job is other persons preference
            //if (person.JobRole.HasFlag(match.JobRole)) return 0.5;        // If my job is other persons job
            return 0;
        }
    }
}