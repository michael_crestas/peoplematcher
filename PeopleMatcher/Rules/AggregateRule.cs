﻿using System;
using System.Linq;
using PeopleMatcher.Annotations;
using PeopleMatcher.Models;

namespace PeopleMatcher.Rules
{
    /// <summary>
    ///     Calculates the weight by aggregating the weights of all of the provided rules.
    ///     The following formula is used:
    ///     Sum(rule.Weighting * rule.GetWeight) // TODO - Update
    /// </summary>
    public class AggregateRule : Rule
    {
        public AggregateRule()
        {
        }

        public AggregateRule([NotNull] params Rule[] rules)
        {
            if (rules == null || rules.Count() < 2)
            {
                throw new ArgumentException(
                    "Cannot use AggregrateRule with less than 2 rules. If only one Rule is required, please use the Rule directly.");
            }

            Weighting = 1.0;
            Rules = rules;
        }

        /// <summary>
        ///     Gets or sets the set of rules to apply for generating a weighting.
        /// </summary>
        /// <value>
        ///     The rules.
        /// </value>
        [NotNull]
        public Rule[] Rules { get; set; }

        public override double GetWeight(Person person, Person match)
        {
            var maxWeight = Rules.Sum(rule => rule.Weighting);
            const int minWeight = 0;
            var weight = Rules.Sum(rule => rule.Weighting*rule.GetWeight(person, match));

            return Utils.Normalise(minWeight, maxWeight, weight);
        }
    }
}