﻿using System;
using System.Diagnostics;
using PeopleMatcher.Models;

namespace PeopleMatcher.Rules
{
    public class YearGroupRule : Rule
    {
        public YearGroupRule()
        {
        }

        public YearGroupRule(double weighting)
        {
            Weighting = weighting;
        }

        public override double GetWeight(Person person, Person match)
        {
            if (person.YearGroup == null || match.YearGroup == null)
            {
                Trace.TraceWarning("{0} does not have a year group", person.YearGroup == null ? person.YearGroup : match.YearGroup);
                return 0;
            }

            var diff = Math.Abs(person.YearGroup.Value - match.YearGroup.Value);
            
            return diff*Math.Exp(-diff)/Math.Exp(-1);
        }
    }
}